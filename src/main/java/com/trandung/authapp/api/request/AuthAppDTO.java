package com.trandung.authapp.api.request;

import lombok.Data;

@Data
public class AuthAppDTO {
    public String app;
    public String key;

    public boolean active;
}
