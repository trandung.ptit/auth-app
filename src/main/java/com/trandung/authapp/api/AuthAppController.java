package com.trandung.authapp.api;


import com.trandung.authapp.api.request.AuthAppDTO;
import com.trandung.authapp.service.AuthAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth-app")
public class AuthAppController {
    @Autowired
    AuthAppService authAppService;

    @PostMapping
    public ResponseEntity<AuthAppDTO> create(@RequestBody AuthAppDTO authAppDTO) {
        boolean result = authAppService.validateActive(authAppDTO);
        authAppDTO.setActive(result);
        return new ResponseEntity<>(authAppDTO, HttpStatus.OK);
    }
}
