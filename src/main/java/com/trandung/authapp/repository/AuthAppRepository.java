package com.trandung.authapp.repository;

import com.trandung.authapp.model.AuthApp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthAppRepository extends JpaRepository<AuthApp, Integer> {
    Optional<AuthApp> findAllByAppAndActiveKeyAndActiveTrue(String app, String key);
}
