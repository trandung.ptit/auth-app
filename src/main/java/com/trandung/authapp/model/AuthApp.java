package com.trandung.authapp.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@Table(name = "auth_app")
@Entity
public class AuthApp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // tự động gen

    @Column
    private String app;

    @Column
    private String note;

    @Column(name = "active_key")
    private String activeKey;

    @Column
    private boolean active;

    @Column(name = "created_date")
    private Instant createdDate;
}
