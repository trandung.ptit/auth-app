package com.trandung.authapp.service;

import com.trandung.authapp.api.request.AuthAppDTO;
import com.trandung.authapp.model.AuthApp;
import com.trandung.authapp.repository.AuthAppRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AuthAppService {
    @Autowired
    AuthAppRepository repository;

    public boolean validateActive(AuthAppDTO authAppDTO) {
        AuthApp authApp = repository.findAllByAppAndActiveKeyAndActiveTrue(authAppDTO.getApp(), authAppDTO.getKey()).orElse(null);
        if (authApp == null) {
            return false;
        }
        return true;
    }
}
